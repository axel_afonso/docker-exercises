# Indica la diferencia entre el uso de la instrucción CMD y ENTRYPOINT (Dockerfile)

La instrucción CMD indica los comandos por defecto al ejecutar el contenedor con 
`docker run`. De esta forma, si no especificamos ningún comando con `docker run`, 
se ejecutarán los comandos indicados en la instrucción CMD.

A diferencia de CMD, ENTRYPOINT indica los comandos que se ejecutarán siempre. 
De esta forma, cualquier comando especificado al correr `docker run` se añadirá 
después de los comandos especificados por la instrucción ENTRYPOINT.

Por lo tanto, si al correr un contenedor queremos lanzar nginx y evitar que se pueda 
usar sh, debemos usar la instrucción `ENTRYPOINT ["nginx"]`, en lugar de  `CMD ["nginx"]`.

Por último, cabe destacar que se pueden usar ENTRYPOINT y CMD en conjunción para 
garantizar que se ejecuta una aplicación con unas opciones por defecto y permitir 
cambiar las opociones sin cambiar la aplicación.
