# Ejercicio 5

El último ejercicio consistía en completar el archivo docker-compose.yml.
Hemos buscado la información necesaria en el docker-compose reference, en la página web de Docker (https://docs.docker.com/compose/compose-file/).
El archivo ```docker-compose.yml``` con la resolución de este ejercicio está incluido en la carpeta ```exercise_5```.

Una vez configurado, realizamos las pruebas según indicado en el enunciado, obteniendo los siguientes resultados:

Contenedores:

  PS C:\Users\axene\OneDrive\Master\Asignaturas\1. Principios y herramientas de desarrollo\3. Ejercicios\docker-exercises\hw-1\exercise_5> docker ps
  CONTAINER ID   IMAGE                 COMMAND                  CREATED          STATUS          PORTS                                            NAMES
  0eed06f3a7ad   kibana:7.9.3          "/usr/local/bin/dumb…"   25 minutes ago   Up 25 minutes   0.0.0.0:5601->5601/tcp                           kibana_container
  59dc1cbffad6   elasticsearch:7.9.3   "/tini -- /usr/local…"   25 minutes ago   Up 25 minutes   0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp   elastic_container

Networks:

PS C:\Users\axene\OneDrive\Master\Asignaturas\1. Principios y herramientas de desarrollo\3. Ejercicios\docker-exercises\hw-1\exercise_5> docker network ls
NETWORK ID     NAME                       DRIVER    SCOPE
ba186a5a6f2a   exercise_5_elastic         bridge    local