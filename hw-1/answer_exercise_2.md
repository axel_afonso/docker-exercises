# Indica la diferencia entre el uso de la instrucción ADD y COPY (Dockerfile)

Tanto ADD como COPY se pueden usar para copiar ficheros y/o directorios de la máquina que ejecuta docker al contenedor cuya imagen se está creando con el Dockerfile.

La principal diferencia entre ambos consiste en que ADD permite copiar desde una url y descomprime automáticamente cualquier archivo comprimido antes de copiarlo.

Por ello, es preferible usar COPY siempre que no se necesite explícitamente ADD, pues puede llevar a confusión y descomprimir archivos que queremos copiar sin descomprimir.
