# Ejercicio 4

Partimos del Dockerfile del ejercicio anterior:

    FROM nginx:1.19.3-alpine

    COPY ./index.html /usr/share/nginx/html

Para realizar el healthcheck, si tuviéramos una API REST lo ideal sería tener un endpoint dedicado al healthcheck. Sin embargo, para este ejercicio de prueba vamos a hacer una petición a index.html. Para ello, usaremos el siguiente comando de curl:

    curl -f http://localhost/ || exit 1

Usaremos el resto de opciones de la instrucción HEALTHCHECK para ajustar los parámetros según se indica en el ejercicio.
Por lo tanto, el Dockerfile resultante sería el siguiente:

    HEALTHCHECK --interval=45s --timeout=5s --retries=2 --start-period=10s CMD curl -f http://localhost/ || exit 1

- ```--interval=45s``` especifica que queremos realizar una comprobación de healthcheck cada 45 segundos.
- ```--timeout=5s``` especifica que queremos esperar un máximo de 5s por la respuesta antes de dar la comprobación por fallida.
- ```--retries=2``` especifica que queremos considerar el container "unhealthy" si fallan dos comprobaciones seguidas.
- ```CMD curl -f http://localhost/ || exit 1``` especifica que el comando para hacer cada comprobación es curl, haciendo una petición a localhost (que por defecto redirige a index.html), y que si falla saldremos con un código de retorno 1 (el necesario para que la instrucción HEALTHCHECK lo interprete como una comprobación fallida). Si nginx nos devuelve el index.html correctamente, el comando saldrá normalmente con un código de retorno 0 (el que la instrucción HEALTHCHECK interpreta como una comprobación exitosa).


### Ajustar el tiempo de espera de la primera prueba (Ejemplo: Si la aplicación del contenedor tarda en iniciarse 10s, configurar el parámetro a 15s)

Según especifica la referencia de Dockerfile:

    start period provides initialization time for containers that need time to bootstrap. Probe failure during that period will not be counted towards the maximum number of retries. However, if a health check succeeds during the start period, the container is considered started and all consecutive failures will be counted towards the maximum number of retries.

Esto quiere decir que ```--start-period=10s``` especifica que queremos esperar 10s antes de contar las comprobaciones que fallen para la cuenta de retries a partir de los 10 primeros segundos. Si hubiera una petición exitosa antes de estos 10s el etado cambiaría igualmente a healthy. En nuestro caso, como tenemos el interval en 45s, la primera petición no se realizará hasta el segundo 45, independientemente del valor de ```--start-period```. Lo único que podríamos conseguir con ```--start-period``` es que si nuestra aplicación tardara más de estos 45s en arrancar, las comprobaciones fallidas anteriores a ```--start-period``` no contaran.

Para comprobar esto, hemos creado ```Dockerfile.start_short``` (en la carpeta ```exercise_4```) y hemos ejecutado ```docker build -t ex_3_short -f Dockerfile.start_short .``` seguido de ```docker run --rm -p 80:80 --name ex_3 ex_3_short```, lo cual ha ejecutado peticiones cada segundo desde 1s después de lanzar el contenedor (lo vemos en los logs), pero no ha cambiado el estado a ```unhealthy``` hasta que el 12s después de iniciar el contenedor (lo vemos con ```docker ps```).

A continuación, hemos creado ```Dockerfile.start_long``` (en la carpeta ```exercise_4```) y hemos ejecutado ```docker build -t ex_3_short -f Dockerfile.start_long .``` seguido de ```docker run --rm -p 80:80 --name ex_3 ex_3_long```, lo cual ha ejecutado peticiones cada 30s desde 30s después de lanzar el contenedor (lo vemos en los logs), y ha cambiado el estado a ```unhealthy``` un minuto después de iniciar el contenedor (lo vemos con ```docker ps```).

Por lo tanto, no tiene sentido ajustar el tiempo de espera de la primera prueba, pues nuestra aplicación tarda menos de los 45 segundos definidos en ```--interval``` en arrancar (arranca en menos de 2 segundos). De todas formas, lo dejaremos en 10s como especifica el enunciado.


## Prueba de funcionamiento

Para probarlo, volvemos a construir la imagen (```docker build -t ex_3_image .```), corremos el contenedor (```docker run -d -p 80:80 --name ex_3 ex_3_image```) y comprobamos que el contenedor empieza con el estado ```starting```, y a los 45 segundos cambia a ```healthy```:

    PS C:\Users\axene\OneDrive\Master\Asignaturas\1. Principios y herramientas de desarrollo\3. Ejercicios\exercise_2> docker ps
    CONTAINER ID   IMAGE        COMMAND                  CREATED         STATUS                           PORTS                NAMES
    c8472ebb5301   ex_3_image   "/docker-entrypoint.…"   2 seconds ago   Up 1 second (health: starting)   0.0.0.0:80->80/tcp   ex_3
    PS C:\Users\axene\OneDrive\Master\Asignaturas\1. Principios y herramientas de desarrollo\3. Ejercicios\exercise_2> docker ps
    CONTAINER ID   IMAGE        COMMAND                  CREATED          STATUS                    PORTS                NAMES
    c8472ebb5301   ex_3_image   "/docker-entrypoint.…"   48 seconds ago   Up 46 seconds (healthy)   0.0.0.0:80->80/tcp   ex_3

Si eliminamos index.html, el estado cambiará a unhealthy tras dos actualizaciones:

    PS C:\Users\axene\OneDrive\Master\Asignaturas\1. Principios y herramientas de desarrollo\3. Ejercicios\exercise_2> docker ps
    CONTAINER ID   IMAGE        COMMAND                  CREATED         STATUS                     PORTS                NAMES
    c8472ebb5301   ex_3_image   "/docker-entrypoint.…"   4 minutes ago   Up 4 minutes (unhealthy)   0.0.0.0:80->80/tcp   ex_3

. 

    {
        "Start": "2021-11-11T14:52:04.9829686Z",
        "End": "2021-11-11T14:52:05.1118387Z",
        "ExitCode": 0,
        "Output": "  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current\n                                 Dload  Upload   Total   Spent    Left  Speed\n\r  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0\u003c!DOCTYPE html\u003e\n\u003chtml\u003e\n\u003chead\u003e\n\u003ctitle\u003eWelcome to nginx!\u003c/title\u003e\n\u003cstyle\u003e\n    body {\n        width: 35em;\n        margin: 0 auto;\n        font-family: Tahoma, Verdana, Arial, sans-serif;\n    }\n\u003c/style\u003e\n\u003c/head\u003e\n\u003cbody\u003e\n\u003ch1\u003eHOMEWORK 1\u003c/h1\u003e\n\u003cp\u003eIf you see this page, the nginx web server is successfully installed and\nworking. Further configuration is required.\u003c/p\u003e\n\n\u003cp\u003eFor online documentation and support please refer to\n\u003ca href=\"http://nginx.org/\"\u003enginx.org\u003c/a\u003e.\u003cbr/\u003e\nCommercial support is available at\n\u003ca href=\"http://nginx.com/\"\u003enginx.com\u003c/a\u003e.\u003c/p\u003e\n\n\u003cp\u003e\u003cem\u003eThank you for using nginx.\u003c/em\u003e\u003c/p\u003e\n\u003c/body\u003e\n\u003c/html\u003e\n\r100   605  100   605    0     0   590k      0 --:--:-- --:--:-- --:--:--  590k\n"
    },
    {
        "Start": "2021-11-11T14:52:50.1253931Z",
        "End": "2021-11-11T14:52:50.2554551Z",
        "ExitCode": 1,
        "Output": "  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current\n                                 Dload  Upload   Total   Spent    Left  Speed\n\r  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0\r  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0\ncurl: (22) The requested URL returned error: 403 Forbidden\n"
    },
    {
        "Start": "2021-11-11T14:53:35.2654828Z",
        "End": "2021-11-11T14:53:35.397002Z",
        "ExitCode": 1,
        "Output": "  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current\n                                 Dload  Upload   Total   Spent    Left  Speed\n\r  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0\r  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0\ncurl: (22) The requested URL returned error: 403 Forbidden\n"
    }