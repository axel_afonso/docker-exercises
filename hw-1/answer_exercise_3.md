# Crea un contenedor con las siguientes especificaciones:

## Utilizar la imagen base NGINX haciendo uso de la versión 1.19.3

Para ello, busqué en DockerHub una imagen de nginx. Podríamos usar la imagen latest, que para este ejercicio de prueba podría valer, pero es mejor usar una versión concreta, porque si usamos latest y actualizan la versión de latest, nuestra aplicación podría no ejecutar la próxima vez que corramos un contenedor. Podría usar la útlima verisón de la imagen base, pero contiene muchas funcionalidades que no necesitamos, y ocupa 52.75MB. Por lo tanto, hemos decidido usar la imagen con tag 1.21.4-alpine, que ocupa 10.05MB y contiene todo lo que necesitamos para este ejercicio.

Cabe destacar que en un entorno de producción almacenaríamos una copia de esta imagen en un repositorio privado, para no depender de Docker Hub, pero para este ejercicio de prueba no es necesario.

Por lo tanto, ejecutaremos el contenedor con el siguiente comando:

    docker run -d -p 80:80 --name ex_3 nginx:1.19.3-alpine

En el comando, usamos -d para ejecutar el contenedor "daemonless". Usamos -p para mappear el puerto 80 del contenedor al puerto 80 de nuestro equipo. Si quisiéramos usar el puerto 8080 de nuestro equipo usaríamos -p 8080:80. Usamos --name para asignarle un nombre al contenedor (por comodidad, para que no lo cree aleatorio).

Usamos directamente la imagen de nginx de Docker Hub, en lugar de usar un Dockerfile, porque hasta ahora no necesitamos hacer ninguna modificación sobre dicha imagen.

## Al acceder a la URL localhost:8080/index.html aparecerá el mensaje HOMEWORK 1

Para ello, podemos entrar en el contenedor con el siguiente comando:

    docker exec -it ex_3 sh

-it (interactive y tty) para abrir una terminal y sh para ejecutar el shell.
Una vez dentro, podemos modificar /usr/share/nginx/html/index.html y Cambiar "Welcome to nginx!" por "HOMEWORK 1" (tenemos que instalar nano primero con ```apk get update && apk add nano```).

Sin embargo, si hacemos esto, al parar y volver a ejecutar el contenedor volvería a poner el mensaje original.

Por lo tanto, creamos una carpeta en nuestro equipo con index.html y el siguiente Dockerfile:

    FROM nginx:1.19.3-alpine

    COPY ./index.html /usr/share/nginx/html

A continuación, construimos la imagen con:

    docker build -t ex_3_imge .

Donde usamos -t para asignarle un nombre a la imagen creada.

Por último, ejecutamos el contenedor con:

    docker run -d -p 80:80 --name ex_3 ex_3_image


## Persistir el fichero index.html en un volumen llamado static_content

Podemos hacerlo con la instrucción VOLUME del Dockerfile, pero esto haría que el Dockerfile dejara de ser "stateless", por lo que usaremos el volumen con el cli de docker:

    docker run --rm -dp 80:80 -v static_content:/usr/share/nginx/html/ --name ex_3 ex_3_image

Hemos añadido ```-v static_content:/usr/share/nginx/html/```, que crea un volumen llamado static_content y guarda el contenido de /usr/share/nginx/html/ en dicho volumen.

De esta forma, si nuestra aplicación crea o modifica ficheros dentro de esa carpeta, éstos se persisten entre reinicios del contenedor.

Lo probamos usando docker exec (igual que en la primera aroximación del apartado anterior), modificando index.html, reiniciando el contenedor y comprobando que la nueva versión de index.html sigue estando ahí.